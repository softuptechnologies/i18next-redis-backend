export const getKey = (lang, namespace, key, { delimiter }) => {
  return `${lang}${delimiter}${namespace}${delimiter}${key}`;
};

export const getNSKey = (namespace, key, { delimiter }) => {
  return `${namespace}${delimiter}${key}`;
};

export const getNSPattern = (lang, namespace, { delimiter }) => {
  return `${lang}${delimiter}${namespace}${delimiter}*`;
};

export const getLangPattern = (lang, { delimiter }) => {
  return `${lang}${delimiter}*`;
};

export default {};
