'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Redis = _interopDefault(require('ioredis'));
var Async = _interopDefault(require('async'));

/**
 * Redis instance
 */
function Redis$1 (opts) {
  var redis = new Redis(opts.url, opts);
  redis.on('error', function (err) {
    console.error(err);
    process.exit(1);
  });
  return redis;
}

var getKey = function getKey(lang, namespace, key, _ref) {
  var delimiter = _ref.delimiter;

  return "" + lang + delimiter + namespace + delimiter + key;
};

var getNSPattern = function getNSPattern(lang, namespace, _ref3) {
  var delimiter = _ref3.delimiter;

  return "" + lang + delimiter + namespace + delimiter + "*";
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function getDefaults() {
  return {
    url: 'redis://0.0.0.0:6379/1',
    delimiter: '.'
  };
}

var Backend = function () {
  function Backend(services) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    classCallCheck(this, Backend);

    this.init(services, options);
    this.type = 'backend';
  }

  /**
   * Initialize backend
   */


  createClass(Backend, [{
    key: 'init',
    value: function init(services) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      this.services = services;
      this.options = _extends({}, getDefaults(), options);
    }

    /**
     * Read multiple keys
     */

  }, {
    key: 'readMulti',
    value: function readMulti(languages, namespaces, cb) {
      var _this = this;

      /**
       * Validations
       */
      if (!cb) throw new Error('No callback provided');
      if (!(languages && namespaces)) throw new Error('Invalid params');
      if (typeof languages === 'string') languages = [languages];
      if (typeof namespaces === 'string') namespaces = [namespaces];
      var client = Redis$1(this.options);
      var keyPatterns = [];
      var nsKeys = [];
      var responseObj = {};
      /**
       * lang-ns mapping
       */
      languages.forEach(function (lang) {
        responseObj[lang] = {};
        namespaces.forEach(function (namespace) {
          responseObj[lang][namespace] = {};
          keyPatterns.push(getNSPattern(lang, namespace, _this.options));
        });
      });
      /**
       * In parallel fetch for each of the mapped values
       * namespaced redis keys
       */
      Async.each(keyPatterns, function (pattern, callback) {
        client.keys(pattern).then(function (foundKey) {
          if (foundKey.length) {
            nsKeys.push(foundKey[0]);
          }
          callback();
        }).catch(function (err) {
          callback(err);
        });
      }, function (err) {
        if (err) {
          client.disconnect();
          return console.error(err);
        }
        /**
         * Get all redis keys matching the namespaced keys
         */
        if (nsKeys.length) {
          client.mget(nsKeys).then(function (res) {
            var tokens = null;
            nsKeys.forEach(function (key, i) {
              if (res[i]) {
                tokens = key.split(_this.options.delimiter);
                responseObj[tokens[0]][tokens[1]] = res[i];
              }
            });
            cb(null, responseObj);
            client.disconnect();
          }).catch(function (error) {
            console.error(error);
            client.disconnect();
            cb(error, null);
          });
        }
        return null;
      });
    }

    /**
     * Read single key
     */

  }, {
    key: 'read',
    value: function read(language, namespace, cb) {
      if (!cb) return new Error('No callback provided');
      if (!(language && namespace)) return new Error('Invalid params');
      var client = Redis$1(this.options);
      var keys = [];
      client.keys(getNSPattern(language, namespace, this.options)).then(function (res) {
        if (res && res.length) {
          res.forEach(function (currentRes) {
            if (currentRes) keys.push(currentRes);
          });
          return client.mget(res);
        }
        return null;
      }).then(function (values) {
        var output = {};
        if (values && values.length) {
          values.forEach(function (value, i) {
            if (value && keys[i]) {
              var tokenized = keys[i].substr(keys[i].indexOf('.') + 1, keys[i].length);
              output[tokenized] = value;
            }
          });
        }
        client.disconnect();
        cb(null, output || {});
      }).catch(function (err) {
        console.error(err);
        client.disconnect();
        cb(err, null);
      });
      return null;
    }

    /**
     * Save a missing entry
     */

  }, {
    key: 'create',
    value: function create(languages, namespace, key, value) {
      var _this2 = this;

      if (!(languages && namespace)) throw new Error('Invalid params');
      if (typeof languages === 'string') languages = [languages];
      var client = Redis$1(this.options);
      languages.forEach(function (lang) {
        client.get(getKey(lang, namespace, key, _this2.options), function (error, val) {
          if (!val) {
            client.set(getKey(lang, namespace, key, _this2.options), value).then(function () {
              client.disconnect();
            }).catch(function (err) {
              console.error(err);
              client.disconnect();
            });
          }
        });
      });
    }

    /**
     * Save an entry
     */

  }, {
    key: 'save',
    value: function save(language, namespace, data) {
      var _this3 = this;

      if (!(language && namespace)) throw new Error('Invalid params');
      var client = Redis$1(this.options);
      var keys = Object.keys(data);
      keys.forEach(function (key) {
        client.set(getKey(language, namespace, key, _this3.options), data[key]);
      });
      client.disconnect();
    }
  }]);
  return Backend;
}();

Backend.type = 'backend';

module.exports = Backend;
