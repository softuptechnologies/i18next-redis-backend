
import { expect } from 'chai';
import RedisBackend from '../src';

describe('redis backend', () => {
  const ns = ['global', 'ns0', 'ns1', 'ns2'];
  const keys = ['key0', 'key1'];
  const vals = ['val0', 'val1', 'val2'];
  const langs = ['en', 'de'];
  let backend;

  before(async () => {
    backend = new RedisBackend();
  });

  it('should create a language entry', async () => {
    backend.create(langs, ns[0], keys[0], vals[0], (err, data) => {
      expect(err).to.be.null;
      expect(data).to.equal('OK');
    });
  });

  it('should get language entries by namespace', async () => {
    backend.read(langs[0], ns[0], (err, data) => {
      expect(err).to.be.null;
      expect(data).to.be.a('object');
    });
  });

  it('should get multiple language entries', async () => {
    backend.readMulti(langs, ns, (err, data) => {
      expect(err).to.be.null;
      expect(data[langs[0]][ns[0]]).to.equal(vals[0]);
    });
  });
});
