import { expect } from 'chai';
import I18Next from 'i18next';
import RedisBackend from '../src';

const backendOptions = {
  url: 'redis://0.0.0.0:6379/1',
  delimiter: '.',
};

describe('i18n using redis backend', async () => {
  before((next) => {
    I18Next
      .use(RedisBackend)
      .init({
        debug: false,
        backend: backendOptions,
        nsSeparator: false,
        keySeparator: false,
        defaultNS: 'global',
        ns: ['global', 'time'],
        fallbackLng: 'en',
        saveMissing: true,
      }, (err) => {
        if (err) throw err;
        next();
      });
  });

  it('should get or put e key', async () => {
    const val = await I18Next.t('key0', {
      defaultValue: 'val0',
      ns: 'global',
      lng: 'en',
    });
    expect(val).to.eq('val0');
  });

  it('should check key existence', async () => {
    const val = await I18Next.exists('key0', {
      ns: 'global',
      lng: 'en',
    });
    expect(val).to.eq(true);
  });

  it('should query a resosurce bundle', async () => {
    await I18Next.loadNamespaces(['global']);
    const resource = await I18Next.getResourceBundle('en', 'global');
    expect(resource.key0).to.equal('val0');
  });
});
