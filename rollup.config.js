import babel from 'rollup-plugin-babel';
import babelrc from 'babelrc-rollup/dist/babelrc-rollup';

export default {
  input: 'src/index.js',
  output: {
    name: 'i18next-redis-backend',
    file: 'dist/index.js',
    sourceMap: true,
    format: 'cjs',
  },
  external: ['async', 'ioredis'],
  plugins: [
    babel(babelrc({
      plugins: [
        'external-helpers',
      ],
    })),
  ],
};
