import Redis from 'ioredis';

/**
 * Redis instance
 */
export default function (opts) {
  const redis = new Redis(opts.url, opts);
  redis.on('error', (err) => {
    console.error(err);
    process.exit(1);
  });
  return redis;
}
