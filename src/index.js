import Async from 'async';
import Redis from './redis';
import { getKey, getNSPattern } from './utils';

function getDefaults() {
  return {
    url: 'redis://0.0.0.0:6379/1',
    delimiter: '.',
  };
}

class Backend {
  constructor(services, options = {}) {
    this.init(services, options);
    this.type = 'backend';
  }

  /**
   * Initialize backend
   */
  init(services, options = {}) {
    this.services = services;
    this.options = {
      ...getDefaults(),
      ...options,
    };
  }

  /**
   * Read multiple keys
   */
  readMulti(languages, namespaces, cb) {
    /**
     * Validations
     */
    if (!cb) throw new Error('No callback provided');
    if (!(languages && namespaces)) throw new Error('Invalid params');
    if (typeof languages === 'string') languages = [languages];
    if (typeof namespaces === 'string') namespaces = [namespaces];
    const client = Redis(this.options);
    const keyPatterns = [];
    const nsKeys = [];
    const responseObj = {};
    /**
     * lang-ns mapping
     */
    languages.forEach((lang) => {
      responseObj[lang] = {};
      namespaces.forEach((namespace) => {
        responseObj[lang][namespace] = {};
        keyPatterns.push(getNSPattern(lang, namespace, this.options));
      });
    });
    /**
     * In parallel fetch for each of the mapped values
     * namespaced redis keys
     */
    Async.each(keyPatterns, ((pattern, callback) => {
      client.keys(pattern).then((foundKey) => {
        if (foundKey.length) {
          nsKeys.push(foundKey[0]);
        }
        callback();
      }).catch((err) => {
        callback(err);
      });
    }), (err) => {
      if (err) {
        client.disconnect();
        return console.error(err);
      }
      /**
       * Get all redis keys matching the namespaced keys
       */
      if (nsKeys.length) {
        client.mget(nsKeys).then((res) => {
          let tokens = null;
          nsKeys.forEach((key, i) => {
            if (res[i]) {
              tokens = key.split(this.options.delimiter);
              responseObj[tokens[0]][tokens[1]] = res[i];
            }
          });
          cb(null, responseObj);
          client.disconnect();
        }).catch((error) => {
          console.error(error);
          client.disconnect();
          cb(error, null);
        });
      }
      return null;
    });
  }

  /**
   * Read single key
   */
  read(language, namespace, cb) {
    if (!cb) return new Error('No callback provided');
    if (!(language && namespace)) return new Error('Invalid params');
    const client = Redis(this.options);
    const keys = [];
    client.keys(getNSPattern(language, namespace, this.options)).then((res) => {
      if (res && res.length) {
        res.forEach((currentRes) => {
          if (currentRes) keys.push(currentRes);
        });
        return client.mget(res);
      }
      return null;
    }).then((values) => {
      const output = {};
      if (values && values.length) {
        values.forEach((value, i) => {
          if (value && keys[i]) {
            const tokenized = keys[i].substr(keys[i].indexOf('.') + 1, keys[i].length);
            output[tokenized] = value;
          }
        });
      }
      client.disconnect();
      cb(null, output || {});
    }).catch((err) => {
      console.error(err);
      client.disconnect();
      cb(err, null);
    });
    return null;
  }

  /**
   * Save a missing entry
   */
  create(languages, namespace, key, value) {
    if (!(languages && namespace)) throw new Error('Invalid params');
    if (typeof languages === 'string') languages = [languages];
    const client = Redis(this.options);
    languages.forEach((lang) => {
      client.get(getKey(lang, namespace, key, this.options), (error, val) => {
        if (!val) {
          client.set(getKey(lang, namespace, key, this.options), value).then(() => {
            client.disconnect();
          }).catch((err) => {
            console.error(err);
            client.disconnect();
          });
        }
      });
    });
  }

  /**
   * Save an entry
   */
  save(language, namespace, data) {
    if (!(language && namespace)) throw new Error('Invalid params');
    const client = Redis(this.options);
    const keys = Object.keys(data);
    keys.forEach((key) => {
      client.set(getKey(language, namespace, key, this.options), data[key]);
    });
    client.disconnect();
  }
}

Backend.type = 'backend';

export default Backend;
